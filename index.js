const apiKey = 'a8d47c70';
const apiURL = 'http://www.omdbapi.com/?apikey=';

async function getMovie() {
  const response = await fetch(`${apiURL}${apiKey}&t=drive`);
  const movie = await response.json();

  return movie;
}

getMovie().then( movie => {
  document.body.innerHTML = `
    <div class="wrapper">
      <div class="movie">
        <span class="year">${movie.Year}</span>
        <h2 class="title">${movie.Title}</h2>
        <span class="genre">${movie.Genre}</span>
        <img class="poster" src="${movie.Poster}" title="${movie.Title}">
        <div class="rating">
          <div class="imdb-rating">
            <span>${movie.Ratings[0].Value}</span>
          </div>
        </div>
        <div class="rating">
          <div class="metacritic-rating">
            <span>${movie.Ratings[1].Value}</span>
          </div>
        </div>
        <div class="rating">
          <div class="rottentomatoes-rating">
            <span>${movie.Ratings[2].Value}</span>
          </div>
        </div>
      </div>
    </div>
  `;
  console.log(movie);
}).catch( error => {
  console.error(error);
});